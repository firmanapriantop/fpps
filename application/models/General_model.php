<?php

	class General_model extends CI_Model 
	{

		function get_data($query)
		{
			return $this->db->query($query);
		}
		
		public function get_all_prov($id=0)
		{
			if ($id<>0)
				$this->db->where('provid', $id);
			
			$query = $this->db->get('ms_wil_provinsi');
				return $query->result_array();
		}
		
		
		public function get_all_kab_by_provId($id=0)
		{	
			if ($id<>0)
				$this->db->where('provid', $id);
			
			$query = $this->db->get('ms_wil_kabupaten');
				
				return $query->result_array();
		}
		
		public function get_all_kec_by_kabId($id=0)
		{	
			if ($id<>0)
				$this->db->where('kabid', $id);
			
			$query = $this->db->get('ms_wil_kecamatan');
				
				return $query->result_array();
		}
		
		public function get_all_desa_by_kecId($id=0)
		{	
			if ($id<>0)
				$this->db->where('kecid', $id);
			
			$query = $this->db->get('ms_wil_desa');
				
				return $query->result_array();
		}
		
		public function get_all_debitur_by_desaId($id=0)
		{	
			if ($id<>0)
				$this->db->where('desaid', $id);
			
			$query = $this->db->get('ms_debitur_vw');
				
				return $query->result_array();
		}
		
		public function get_all_role()
		{	
					
			$query = $this->db->get('sys_role');
				
				return $query->result_array();
		}
		
		public function save_data($nama_table, $primary_key, $id = 0, $data)
		{
			if ($id == 0) {
				return $this->db->insert($nama_table, $data);
			} else {
				$this->db->where($primary_key, $id);
				return $this->db->update($nama_table, $data);
			}
		}
	}
	
	

?>