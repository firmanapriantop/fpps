<div class="m-portlet__head">
    <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
            <span class="m-portlet__head-icon m--hide">
                <i class="la la-gear"></i>
            </span>
            <h3 class="m-portlet__head-text">
                Perbandingan Ajuan Kredit dengan Realisasi
            </h3>
        </div>
    </div>
</div>
<div class="m-portlet__body">
    <div id="myfirstchart" style="height:350px;">
    </div>
</div>

<script src="<?= base_url()?>/theme/vendors/jquery/dist/jquery.js" type="text/javascript"></script>
<script src="<?= base_url()?>/assets/muds.js" type="text/javascript"></script>

<script>
    $(document).ready(function(){
		var data = [
			<?php foreach($hasil->result() as $row): ?>
				{ y: '<?= $row->bulan2 ?>', a: <?= $row->ajuan_kredit ?>, b: <?= $row->realisasi ?>},
			<?php endforeach; ?>
		];
		
		new Morris.Bar({
			element: 'myfirstchart',
			data: data,
			xkey: 'y',
			ykeys: ['a', 'b'],
			labels: ['Ajuan Kredit', 'Realisasi']
		});

	});
</script> 