<div class="col-xl-3">
	<div class="m-portlet m--bg-accent m-portlet--bordered-semi m-portlet--skin-dark m-portlet--full-height ">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Jumlah FPPS
					</h3>
				</div>
			</div>
		</div>
		<div class="m-portlet__body">
			<div class="m-widget7 m-widget7--skin-dark">
                <h1 class="m-widget27__title m--font-light text-right">
                    <span><?= $jlh_user ?></span>
                </h1>
			</div>
		</div>
	</div>
</div>

<div class="col-xl-3">
	<div class="m-portlet m--bg-primary m-portlet--bordered-semi m-portlet--skin-dark m-portlet--full-height ">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Jumlah Usulan
					</h3>
				</div>
			</div>
		</div>
		<div class="m-portlet__body">
			<div class="m-widget7 m-widget7--skin-dark">
                <h1 class="m-widget27__title m--font-light text-right">
                    <span><?= $jlh_usulan ?></span>
                </h1>
			</div>
		</div>
	</div>
</div>

<div class="col-xl-3">
	<div class="m-portlet m--bg-success m-portlet--bordered-semi m-portlet--skin-dark m-portlet--full-height ">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Jumlah Ajuan Kredit
					</h3>
				</div>
			</div>
		</div>
		<div class="m-portlet__body">
			<div class="m-widget7 m-widget7--skin-dark">
                <h1 class="m-widget27__title m--font-light text-right">
                    <span><?= number_format($jlh_ajuan_kredit, 2) ?></span>
                </h1>
			</div>
		</div>
	</div>
</div>

<div class="col-xl-3">
	<div class="m-portlet m--bg-danger m-portlet--bordered-semi m-portlet--skin-dark m-portlet--full-height ">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Jumlah Realisasi
					</h3>
				</div>
			</div>
		</div>
		<div class="m-portlet__body">
			<div class="m-widget7 m-widget7--skin-dark">
                <h1 class="m-widget27__title m--font-light text-right">
                    <span><?= number_format($jlh_realisasi, 2) ?></span>
                </h1>
			</div>
		</div>
	</div>
</div>