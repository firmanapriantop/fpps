<div class="m-subheader ">
	<div class="d-flex align-items-center">
		<div class="mr-auto">
			<h3 class="m-subheader__title text-capitalize">Dashboard</h3>
		</div>
		<div class="col-md-2">
			<select class="form-control m-select2" id="tahun" name="tahun">
				<?php 
					foreach ($tahun->result() as $row):
						echo '<option value="'.$row->tahun.'">'.$row->tahun.'</option>';
					endforeach;
				?>
			</select>
		</div>
	</div>
</div>


<!-- END: Subheader -->
<div class="m-content">



	<!--begin:: Widgets/Stats-->
	<div class="row" id="info_box">
		
	</div>

	<div class="m-portlet m-portlet--tab" id="bar_chart">
		
	</div>

	<div class="m-portlet m-portlet--tab" id="line_chart">
		
	</div>

</div>

<script src="<?= base_url()?>/theme/vendors/jquery/dist/jquery.js" type="text/javascript"></script>
<script src="<?= base_url()?>/assets/muds.js" type="text/javascript"></script>

<script>
    $(document).ready(function(){
		$("#tahun").select2({
			dropdownAutoWidth: true
		});
		var tahun_s = $("#tahun").val();
		LoadData("<?= base_url('dashboard/load_info_box/') ?>"+tahun_s, "info_box");
		LoadData("<?= base_url('dashboard/load_bar_chart/') ?>"+tahun_s, "bar_chart");
		LoadData("<?= base_url('dashboard/load_line_chart/') ?>"+tahun_s, "line_chart");


		$('#tahun').change(function(){
            LoadData("<?= base_url('dashboard/load_info_box/') ?>"+$(this).val(), "info_box");
			LoadData("<?= base_url('dashboard/load_bar_chart/') ?>"+$(this).val(), "bar_chart");
			LoadData("<?= base_url('dashboard/load_line_chart/') ?>"+$(this).val(), "line_chart");
        });

	});
</script> 