<!--begin:: Global Mandatory Vendors -->
		<link href="<?= base_url()?>/theme/vendors/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" type="text/css" />

		<!--end:: Global Mandatory Vendors -->

		<!--begin:: Global Optional Vendors -->
		<link href="<?= base_url()?>/theme/vendors/tether/dist/css/tether.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url()?>/theme/vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url()?>/theme/vendors/bootstrap-datetime-picker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url()?>/theme/vendors/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url()?>/theme/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url()?>/theme/vendors/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url()?>/theme/vendors/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url()?>/theme/vendors/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url()?>/theme/vendors/select2/dist/css/select2.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url()?>/theme/vendors/nouislider/distribute/nouislider.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url()?>/theme/vendors/owl.carousel/dist/assets/owl.carousel.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url()?>/theme/vendors/owl.carousel/dist/assets/owl.theme.default.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url()?>/theme/vendors/ion-rangeslider/css/ion.rangeSlider.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url()?>/theme/vendors/ion-rangeslider/css/ion.rangeSlider.skinFlat.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url()?>/theme/vendors/dropzone/dist/dropzone.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url()?>/theme/vendors/summernote/dist/summernote.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url()?>/theme/vendors/bootstrap-markdown/css/bootstrap-markdown.min.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url()?>/theme/vendors/animate.css/animate.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url()?>/theme/vendors/toastr/build/toastr.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url()?>/theme/vendors/jstree/dist/themes/default/style.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url()?>/theme/vendors/morris.js/morris.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url()?>/theme/vendors/chartist/dist/chartist.min.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url()?>/theme/vendors/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url()?>/theme/vendors/socicon/css/socicon.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url()?>/theme/vendors/vendors/line-awesome/css/line-awesome.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url()?>/theme/vendors/vendors/flaticon/css/flaticon.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url()?>/theme/vendors/vendors/metronic/css/styles.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url()?>/theme/vendors/vendors/fontawesome5/css/all.min.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url()?>theme/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
		
<!-- bentrok dengan datatable.bundle.css
		<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />-->
		<link href="<?= base_url()?>/theme/assets/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
		<!--end:: Global Optional Vendors -->

		<!--begin::Global Theme Styles -->
		<link href="<?= base_url()?>/theme/assets/demo/base/style.bundle.css" rel="stylesheet" type="text/css" />

		<!--RTL version:<link href="assets/demo/base/style.bundle.rtl.css" rel="stylesheet" type="text/css" />-->

		<!--end::Global Theme Styles -->

		<!--begin::Page Vendors Styles -->
		<link href="<?= base_url()?>/theme/assets/vendors/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />

		<!--RTL version:<link href="assets/vendors/custom/fullcalendar/fullcalendar.bundle.rtl.css" rel="stylesheet" type="text/css" />-->

		<!--end::Page Vendors Styles -->