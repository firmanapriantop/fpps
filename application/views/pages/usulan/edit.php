<?php //print_r($get_kab);
	$provid = $data->provid;
	$kabid = $data->kabid;
	$kecid = $data->kecid;
	$desaid = $data->desaid;
	
	$debiturid=$data->debiturid;
	$komoditasid=$data->komoditasid;
	$agunan=$data->agunan;
	$ajuan=$data->ajuan_kredit;
	$jangka=$data->jangka_kredit;
	
	$pemanfaatan=$data->pemanfaatanid;
	$bankid=$data->bankid;
	$status=$data->status;
	$realisasi=$data->realisasi;
	$noSpk=$data->no_spk;
	$keterangan=$data->keterangan;
	$tgl = TglUK($data->periode);

?>

<div class="m-content">

						<!--begin::Portlet-->
						<div class="m-portlet">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text">
											Tambah Usulan
										</h3>
									</div>
								</div>
							</div>

							<!--begin::Form-->
							<form class="m-form m-form--fit m-form--label-align-right" id="m_form_1" method="POST" action="<?php echo base_url(); ?>usulan/save/<?=$data->usulanid?>">
								<div class="m-portlet__body">
									<div class="m-form__content">
										<div class="m-alert m-alert--icon alert alert-danger m--hide" role="alert" id="m_form_1_msg">
											<div class="m-alert__icon">
												<i class="la la-warning"></i>
											</div>
											<div class="m-alert__text">
												Silahkan perbaiki dan lengkapi data anda!
											</div>
											<div class="m-alert__close">
												<button type="button" class="close" data-close="alert" aria-label="Close">
												</button>
											</div>
										</div>
									</div>
									
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Provinsi *</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<select class="form-control m-select2" id="m_select2_1" name="provinsi">
											
												<option value="">Pilih Provinsi</option>
												<?php foreach($get_prov as $row => $data){ ?>
													<option value="<?php echo $data['provid']; ?>" <?php echo ($data['provid']==$provid ? 'selected' : '');?>><?php echo $data['name']; ?></option>
												<?php } ?>
											</select>
											<span class="m-form__help">Pilih Provinsi</span>
										</div>
									</div>
									
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Kabupaten *</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<select class="form-control m-select2" id="m_select2_2" name="kabupaten">
											
												<option value="">Pilih Kabupaten</option>
												<?php foreach($get_kab as $row => $data){ ?>
													<option value="<?php echo $data['kabid']; ?>" <?php echo ($data['kabid']==$kabid ? 'selected' : '');?>><?php echo $data['name']; ?></option>
												<?php } ?>
											</select>
											<span class="m-form__help">Pilih Kabupaten</span>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Kecamatan *</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<select class="form-control m-select2" id="m_kecamatan" name="kecamatan">
											
												<option value="">Pilih Kecamatan</option>
												<?php foreach($get_kec as $row => $data){ ?>
													<option value="<?php echo $data['kecid']; ?>" <?php echo ($data['kecid']==$kecid ? 'selected' : '');?>><?php echo $data['name']; ?></option>
												<?php } ?>
											</select>
											<span class="m-form__help">Pilih Kecamatan</span>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Desa *</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<select class="form-control m-select2" id="m_desa" name="desa">
											
												<option value="">Pilih Desa</option>
												<?php foreach($get_desa as $row => $data){ ?>
													<option value="<?php echo $data['desaid']; ?>" <?php echo ($data['desaid']==$desaid ? 'selected' : '');?>><?php echo $data['name']; ?></option>
												<?php } ?>
											</select>
											<span class="m-form__help">Pilih Desa</span>
										</div>
									</div>
									
									<div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space"></div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Debitur *</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<select class="form-control m-select2" id="m_debitur" name="debitur">
											
												<option value="">Pilih Debitur</option>
												<?php foreach($get_debitur as $row => $data){ ?>
													<option value="<?php echo $data['debiturid']; ?>" <?php echo ($data['debiturid']==$debiturid ? 'selected' : '');?>><?php echo $data['nama']; ?></option>
												<?php } ?>
											</select>
											<span class="m-form__help">Pilih Debitur</span>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Komoditas *</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<select class="form-control m-select2" id="m_komoditas" name="komoditas">
											
												<option value="">Pilih Komoditas</option>
												<?php foreach($get_komoditas as $row => $data){ ?>
													<option value="<?php echo $data['komoditasid']; ?>" <?php echo ($data['komoditasid']==$komoditasid ? 'selected' : '');?>><?php echo $data['Jenis']; ?></option>
												<?php } ?>
											</select>
											<span class="m-form__help">Pilih Komoditas</span>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Pemanfaatan Kredit *</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<select class="form-control m-select2" id="m_pemanfaatan" name="pemanfaatan">
											
												<option value="">Pilih Pemanfaatan</option>
												<?php foreach($get_kat_pemanfaatan as $row => $data){ ?>
													<option value="<?php echo $data['pemanfaatanid']; ?>" <?php echo ($data['pemanfaatanid']==$pemanfaatan ? 'selected' : '');?>><?php echo $data['kategori']; ?></option>
												<?php } ?>
											</select>
											<span class="m-form__help">Pilih kategori pemanfaatan kredit</span>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Agunan *</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<input type="text" class="form-control m-input" name="agunan" placeholder="Agunan " data-toggle="m-tooltip" value="<?=$agunan?>">
											<span class="m-form__help">Isi agunan</span>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Ajuan Kredit *</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<input type="text" class="form-control m-input" name="ajuan" placeholder="Ajuan Kredit " data-toggle="m-tooltip" value="<?=$ajuan?>">
											<span class="m-form__help">Isi ajuan kredit</span>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Jangka Kredit *</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<input type="text" class="form-control m-input" name="jangka" placeholder="Jangka Kredit " data-toggle="m-tooltip" value="<?=$jangka?>">
											<span class="m-form__help">Isi jangka kredit dalam bulan</span>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Temu Pembiayaan *</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<div class="input-group date">
												<input type="text" class="form-control m-input" readonly id="m_datepicker_3" placeholder="Periode" name="periode" value="<?=$tgl?>/>
												<div class="input-group-append">
													<span class="input-group-text">
														<i class="la la-calendar"></i>
													</span>
												</div>
											</div>
											<span class="m-form__help">Isi tanggal periode temu pembiayaan</span>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Bank *</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
												
											<select class="form-control m-select2" id="m_bank" name="bank">
												<option value="">Pilih Bank</option>
												<?php foreach($get_bank as $row => $data){ ?>
													<option value="<?php echo $data['bankid']; ?>" <?php echo ($data['bankid']==$bankid ? 'selected' : '');?>><?php echo $data['nama']; ?></option>
												<?php } ?>
											</select>
											<span class="m-form__help">Pilih Bank</span>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Status *</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
												
											<select class="form-control m-select2" id="m_select2_4" name="status">
												<option value="">Pilih Status</option>
												<option value="1" <?php echo ($status==1 ? 'selected' : '');?>>Diterima</option>
												<option value="2" <?php echo ($status==2 ? 'selected' : '');?>>Ditolak</option>
											</select>
											<span class="m-form__help">Pilih Status</span>
										</div>
									</div>
									
									<div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space"></div>
									
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Realisasi *</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<input type="text" class="form-control m-input" name="realisasi" id="m_realisasi" placeholder="Realisasi " data-toggle="m-tooltip" <?php echo ($status==1 ? '' : 'disabled');?> value="<?=$realisasi;?>">
											<span class="m-form__help">Isi realisasi</span>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">No. SPK *</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<input type="text" class="form-control m-input" name="noSPK" id="m_noSPK" placeholder="Nomor SPK " data-toggle="m-tooltip" <?php echo ($status==1 ? '' : 'disabled');?> value="<?=$noSpk;?>">
											<span class="m-form__help">Isi nomor SPK</span>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Keterangan</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<textarea class="form-control m-input" name="keterangan" placeholder="Keterangan" ></textarea>
											<span class="m-form__help">Keterangan</span>
										</div>
									</div>
									
								</div>
								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions m-form__actions">
										<div class="row">
											<div class="col-lg-9 ml-lg-auto">
												<button type="submit" class="btn btn-success">Simpan</button>
												<button type="reset" class="btn btn-secondary" id="batal">Batal</button>
											</div>
										</div>
									</div>
								</div>
							</form>

							<!--end::Form-->
						</div>
</div>


<script src="<?= base_url()?>/theme/vendors/jquery/dist/jquery.js" type="text/javascript"></script>

<script  type="text/javascript">

//==untuk validasi form
var FormControls = function () {
    //== Private functions
 
        var demo1 = function () {
        $( "#m_form_1" ).validate({
            // define validation rules
            rules: {
                provinsi: {
                    required: true
                },
                kabupaten: {
                    required: true
                },				
                kecamatan: {
                    required: true
                },
                desa: {
                    required: true
                },
				debitur: {
                    required: true
                },
				komoditas: {	
                    required: true
                },
				pemanfaatan: {	
                    required: true
                },
				agunan: {
                    required: true
                },
				ajuan: {
                    required: true,
					digits:true
                },				
				jangka: {
                    required: true
                },
				periode: {
                    required: true
                },
				bank: {
                    required: true
                },
				noSPK: {
                    required: true
                },
                realisasi: {
                    required: true,
					digits:true
                },
				status: {
                    required: true
                },
            },
            
			//override massage
			messages: {
				provinsi: "Silahkan pilih provinsi",
				kabupaten: "Silahkan pilih kabupaten",
				kecamatan: "Silahkan pilih kecamatan",
				desa: "Silahkan pilih desa",
				debitur: "Silahkan pilih debitur/pelaku usaha",
				agunan: "Silahkan isikan agunan",
				komoditas: "Silahkan pilih komoditas",
				pemanfaatan:"Silahkan kategori pemanfaatan kredit",
				ajuan: {
					required: "Silahkan isi ajuan",
					digits: "Silahkan isi dengan angka",
				}, 
				jangka: "Silahkan isi jangka kredit",
				realisasi: {
					required: "Silahkan isi realisasi",
					digits: "Silahkan isi dengan angka",
				},
				noSPK: "Silahkan isi nomor SPK",
				status: "Silahkan pilih status",
			},
		
            //display error alert on form submit  
            invalidHandler: function(event, validator) {     
                var alert = $('#m_form_1_msg');
                alert.removeClass('m--hide').show();
                mUtil.scrollTop();
            },

            submitHandler: function (form) {
                form[0].submit(); // submit the form
				
            }
        });       
    }
 
		
	return {
        // public functions
        init: function() {
            demo1(); 
        }
    };
}();

//== untuk option with search
var Select2 = function() {
    //== Private functions
    var demos = function() {

        // basic
        $('#m_select2_1').select2({
            placeholder: "Pilih Provinsi",
            allowClear: true
        });

		// basic
        $('#m_select2_2').select2({
            placeholder: "Pilih Kabupaten",
            allowClear: true
        });
		
		// basic
        $('#m_kecamatan').select2({
            placeholder: "Pilih Kecamatan",
            allowClear: true
        });
		// basic
        $('#m_desa').select2({
            placeholder: "Pilih Desa",
            allowClear: true
        });
		
		// basic
        $('#m_debitur',).select2({
            placeholder: "Pilih Debitur",
            allowClear: true
        });
		// basic
        $('#m_komoditas',).select2({
            placeholder: "Pilih Komoditas",
            allowClear: true
        });
		
		// basic
        $('#m_pemanfaatan',).select2({
            placeholder: "Pilih Pemanfaatan Kredit",
            allowClear: true
        });
		
		// basic
        $('#m_bank',).select2({
            placeholder: "Pilih Bank",
            allowClear: true
        });
		// basic
        $('#m_select2_4',).select2({
            placeholder: "Pilih Status",
            allowClear: true
        });
    }


    //== Public functions
    return {
        init: function() {
            demos();
        }
    };
}();

	$(document).ready(function(){
		function get_kab($id,$param){
			$.ajax({
						type: "get",
						url: "<?= base_url() ?>user/get_data_by_id_json",
						data: "id=" + $id + "&param=" + $param,
						success: function (response) {
							//get_csrf();
							
							//console.log(response);
							var data= JSON.parse(response);
							
							if($param=='kabupaten'){
								var $el = $("#m_select2_2");
								$el.empty(); // remove old options
								$el.append('<option value="none">Pilih Kabupaten</option>');
								$.each(data,function(i,value){
									$el.append('<option value="'+value.kabid+'">'+value.name+'</option>');
								});
							}else if ($param=='kecamatan'){
								var $el = $("#m_kecamatan");
								$el.empty(); // remove old options
								$el.append('<option value="none">Pilih Kecamatan</option>');
								$.each(data,function(i,value){
									$el.append('<option value="'+value.kecid+'">'+value.name+'</option>');
								});
							}else if ($param=='desa'){
								var $el = $("#m_desa");
								$el.empty(); // remove old options
								$el.append('<option value="none">Pilih Desa</option>');
								$.each(data,function(i,value){
									$el.append('<option value="'+value.desaid+'">'+value.name+'</option>');
								});
							};
						}
					});
			
		}
		
		function get_debitur($id){
			$.ajax({
						type: "get",
						url: "<?= base_url() ?>pelaku_usaha/get_data_by_id_json",
						data: "desaid=" + $id,
						success: function (response) {
							//get_csrf();
							
							//console.log(response);
							var data= JSON.parse(response);
							
							var $el = $("#m_debitur");
							$el.empty(); // remove old options
							$el.append('<option value="none">Pilih Debitur</option>');
							$.each(data,function(i,value){
								$el.append('<option value="'+value.debiturid+'">'+value.nama+' ('+value.nik+')'+'</option>');
							});
							
						}
					});
			
		}
		
		//panggil list kabupaten
		$('#m_select2_1').change(function(e){
			var thisId=$(this).val();
			//alert(thisId);
			
			get_kab(thisId,"kabupaten")
			
		});
		// panggil list kecamatan
		$('#m_select2_2').change(function(e){
			var thisId=$(this).val();
			//alert(thisId);
			
			get_kab(thisId,"kecamatan")
			
		});
		//panggil lis desa
		$('#m_kecamatan').change(function(e){
			var thisId=$(this).val();
			//alert(thisId);
			
			get_kab(thisId,"desa")
			
		});
		
		
		//panggil list debitur
		$('#m_desa').change(function(e){
			var thisId=$(this).val();
			//alert(thisId);
			
			get_debitur(thisId)
			
		});
		
		$('#m_select2_4').change(function(e){
			var thisId=$(this).val();
			//alert(thisId);
			if(thisId=="1"){
				//alert(thisId);
				$('#m_realisasi')[0].disabled=false;
				$('#m_noSPK')[0].disabled=false;
			}else{
				$('#m_realisasi')[0].disabled=true;
				$('#m_noSPK')[0].disabled=true;
			}
		});
		
		$('#batal').click(function(){
			window.location.href='<?= base_url() ?>user';
		});
		
		FormControls.init();
		Select2.init();
        
    });
</script>
