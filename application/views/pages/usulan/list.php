<!--begin: Datatable -->
<table class="table table-striped- table-bordered table-hover" id="tabel">
    <thead>
						<tr>
							<th></th>
							<th>NIK</th>
							<th>Nama</th>
							<th>Pekerjaan</th>
							<th>Pemanfaatan</th>
							<th>Ajuan Kredit</th>
							<th>Realisasi</th>
							<th>Jangka Kredit (Bulan)</th>
							<th>Desa</th>
							<th>Kecamatan</th>
							<th>Kabupaten</th>
							<th>Provinsi</th>
							<th>Status</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php $no=0; foreach ($data->result() as $row): $no++; ?>
						<tr>
							<td><?= $no; ?></td>
							<td><?= $row->nik; ?></td>
							<td><?= $row->nama; ?></td>
							<td><?= $row->pekerjaan; ?></td>
							<td><?= $row->pemanfaatan; ?></td>
							<td><?= $row->ajuan_kredit; ?></td>
							<td><?= $row->realisasi; ?></td>
							<td><?= $row->jangka_kredit; ?></td>
							<td><?= $row->desa; ?></td>
							<td><?= $row->kecamatan; ?></td>
							<td><?= $row->kabupaten; ?></td>
							<td><?= $row->provinsi; ?></td>
							<td>
								<?php 
									if ($row->status_text == 'Diterima') {
										echo '<span class="m-badge m-badge--success m-badge--wide">'.$row->status_text.'</span>';
									} else {
										echo '<span class="m-badge m-badge--danger m-badge--wide">'.$row->status_text.'</span>';
									}
								?>
							</td>
							<td>
								<span class="dropdown">
									<a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="true"><i class="la la-ellipsis-h"></i></a>
									<div class="dropdown-menu dropdown-menu-right">
										<a class="dropdown-item" href="<?= base_url('usulan/edit/'.$row->usulanid) ?>" ><i class="la la-edit"></i> Edit</a>
										<a class="dropdown-item text-danger hapus" href="#" data-toggle="modal" data-target="#confirm-delete" data-id="<?=$row->usulanid?>" full-name="<?=$row->nama?>"><i class="la 	la-trash text-danger"></i> Hapus</a>
									</div>
								</span>
							</td>
						</tr>
						<?php endforeach; ?>
					</tbody>
</table>



<script>
	$(document).ready(function(){
		$('#tabel thead tr').clone(true).appendTo( '#tabel thead' );
        $('#tabel thead tr:eq(1) th').each( function (i) {
			var title = $(this).text();
			switch(title){
				case"NIK":
				case"Nama":
				case"Pekerjaan":
				case"Pemanfaatan" :
				case"Ajuan Kredit":
				case"Realisasi":
				case"Jangka Kredit (Bulan)":
				case"Desa":
				case"Kecamatan":
				case"Kabupaten":
			    case"Provinsi":
				case "Status":
					$(this).html( '<input type="text" class="form-control form-control-sm form-filter m-input" placeholder="Search '+title+'" />' );
				break;
			};

			$( 'input', this ).on( 'keyup change', function () {
                if ( table.column(i).search() !== this.value ) {
                        table
                            .column(i)
                            .search( this.value )
                            .draw();
                }
			});
        });
 
        var table = $('#tabel').DataTable({
            orderCellsTop: true,
            fixedHeader: true,
            responsive:!0,
            "columnDefs": [{
                "targets"  : -1,
                "orderable": false
            }]
		});
		
		
			
		$('#confirm-delete').on('show.bs.modal', function (e) {
			// get information to update quickly to modal view as loading begins
			var opener=e.relatedTarget;//this holds the element who called the modal
			   
			//we get details from attributes
			var id=$(opener).attr('data-id');
			var fullname=$(opener).attr('full-name');
			
			$('.title').text(' ' + fullname + ' ');
			$('.confirm-hapus').data('id', id);
		});
		
		$('.confirm-hapus').click(function(){
			var ID = $(this).data('id');
			//alert(ID);
			$.ajax({
				url: "<?php echo base_url(); ?>usulan/delete/"+ID,
				type: "post",
				data:ID,
				success: function (data) {
					$("#confirm-delete").modal('hide');
					//$("#deleted").modal('show');
					LoadData("<?php echo base_url(); ?>usulan/show_data", "tabelx");
				}
			  });
		});
  });
</script>
