<?php //print_r($get_kab);
	$nik = $data->nik;
	$nama = $data->nama;
	$tgl = TglUK($data->tgl_lahir);
	$jk = $data->jenis_kelamin;
	$maritalid = $data->maritalid;
	$pendidikanid = $data->pendidikanid;
	$pekerjaan = $data->pekerjaan;
	$provid = $data->provid;
	$kabid = $data->kabid;
	$kecid = $data->kecid;
	$desaid = $data->desaid;
	$npwp = $data->npwp;
	$alamat = $data->alamat_lengkap;
	
?>

<div class="m-content">

						<!--begin::Portlet-->
						<div class="m-portlet">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text">
											Tambah Pelaku Usaha
										</h3>
									</div>
								</div>
							</div>

							<!--begin::Form-->
							<form class="m-form m-form--fit m-form--label-align-right" id="m_form_1" method="POST" action="<?php echo base_url(); ?>pelaku_usaha/save/<?=$data->debiturid?>">
								<div class="m-portlet__body">
									<div class="m-form__content">
										<div class="m-alert m-alert--icon alert alert-danger m--hide" role="alert" id="m_form_1_msg">
											<div class="m-alert__icon">
												<i class="la la-warning"></i>
											</div>
											<div class="m-alert__text">
												Silahkan perbaiki dan lengkapi data anda!
											</div>
											<div class="m-alert__close">
												<button type="button" class="close" data-close="alert" aria-label="Close">
												</button>
											</div>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">NIK *</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<div class="input-group">
												<input type="text" class="form-control m-input" name="nik" placeholder="NIK" value="<?=$nik?>">
											</div>
											<span class="m-form__help">Isi NIK</span>
										</div>
									</div>
									
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Nama Lengkap *</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<input type="text" class="form-control m-input" name="nama" placeholder="Nama Lengkap" data-toggle="m-tooltip" value="<?=$nama?>" >
											<span class="m-form__help">Isi nama lengkap</span>
										</div>
									</div>
									
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Tanggal Lahir *</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<div class="input-group date">
												<input type="text" class="form-control m-input" readonly id="m_datepicker_3" placeholder="Tanggal Lahir" name="tanggal" value="<?=$tgl?>"/>
												<div class="input-group-append">
													<span class="input-group-text">
														<i class="la la-calendar"></i>
													</span>
												</div>
											</div>
											<span class="m-form__help">Isi Tanggal Lahir</span>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Jenis Kelamin *</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<select class="form-control m-select2" id="jenis_kelamin" name="jk">
												<option value="">Pilih Status</option>
												<option value="1"<?php echo ($jk='1' ? 'selected' : '');?>>Laki-Laki</option>
												<option value="2"<?php echo ($jk='1' ? 'selected' : '');?>>Perempuan</option>
											</select>
											<span class="m-form__help">Pilih Jenis Kelamin</span>
										</div>
									</div>
									
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Marital Status *</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<select class="form-control m-select2" id="marital_status" name="marital">
											
												<option value="">Pilih Marital Status</option>
												<?php foreach($get_marital as $row => $data){ ?>
													<option value="<?php echo $data['maritalid']; ?>" <?php echo ($data['maritalid']==$maritalid ? 'selected' : '');?>><?php echo $data['status']; ?></option>
												<?php } ?>
											</select>
											<span class="m-form__help">Pilih Marital Status</span>
										</div>
									</div>
									
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Pendidikan *</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<select class="form-control m-select2" id="m_pendidikan" name="pendidikan">
											
												<option value="">Pilih Pendidikan</option>
												<?php foreach($get_pendidikan as $row => $data){ ?>
													<option value="<?php echo $data['pendidikanid']; ?>" <?php echo ($data['pendidikanid']==$pendidikanid ? 'selected' : '');?>><?php echo $data['singkatan']; ?></option>
												<?php } ?>
											</select>
											<span class="m-form__help">Pilih Pendidikan</span>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Pekerjaan *</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<input type="text" class="form-control m-input" name="pekerjaan" placeholder="Pekerjaan" data-toggle="m-tooltip" value="<?=$pekerjaan?>">
											<span class="m-form__help">Isi Pekerjaan</span>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">NPWP </label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<input type="text" class="form-control m-input" name="npwp" placeholder="Nomor NPWP" data-toggle="m-tooltip" value="<?=$npwp?>">
											<span class="m-form__help">Isi Nomor NPWP</span>
										</div>
									</div>
									<div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space"></div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Provinsi *</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<select class="form-control m-select2" id="m_select2_1" name="provinsi">
											
												<option value="">Pilih Provinsi</option>
												<?php foreach($get_prov as $row => $data){ ?>
													<option value="<?php echo $data['provid']; ?>" <?php echo ($data['provid']==$provid ? 'selected' : '');?>><?php echo $data['name']; ?></option>
												<?php } ?>
											</select>
											<span class="m-form__help">Pilih Provinsi</span>
										</div>
									</div>
									
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Kabupaten *</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<select class="form-control m-select2" id="m_select2_2" name="kabupaten">
											
												<option value="">Pilih Kabupaten</option>
												<?php foreach($get_kab as $row => $data){ ?>
													<option value="<?php echo $data['kabid']; ?>" <?php echo ($data['kabid']==$kabid ? 'selected' : '');?>><?php echo $data['name']; ?></option>
												<?php } ?>
											</select>
											<span class="m-form__help">Pilih Kabupaten</span>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Kecamatan *</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<select class="form-control m-select2" id="m_kecamatan" name="kecamatan">
											
												<option value="">Pilih Kecamatan</option>
												<?php foreach($get_kec as $row => $data){ ?>
													<option value="<?php echo $data['kecid']; ?>" <?php echo ($data['kecid']==$kecid ? 'selected' : '');?>><?php echo $data['name']; ?></option>
												<?php } ?>
											</select>
											<span class="m-form__help">Pilih Kecamatan</span>
										</div>
									</div>
									
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Desa *</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<select class="form-control m-select2" id="m_desa" name="desa">
											
												<option value="">Pilih Desa</option>
												<?php foreach($get_desa as $row => $data){ ?>
													<option value="<?php echo $data['desaid']; ?>" <?php echo ($data['desaid']==$desaid ? 'selected' : '');?>><?php echo $data['name']; ?></option>
												<?php } ?>
											</select>
											<span class="m-form__help">Pilih Desa</span>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Alamat Lengkap *</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<textarea class="form-control m-input" name="alamat" placeholder="Alamat Lengkap"><?=$alamat;?></textarea>
											<span class="m-form__help">Isi Alamat Lengkap</span>
										</div>
									</div>
									
								</div>
								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions m-form__actions">
										<div class="row">
											<div class="col-lg-9 ml-lg-auto">
												<button type="submit" class="btn btn-success">Simpan</button>
												<button type="reset" class="btn btn-secondary" id="batal">Batal</button>
											</div>
										</div>
									</div>
								</div>
							</form>

							<!--end::Form-->
						</div>
</div>


<script src="<?= base_url()?>/theme/vendors/jquery/dist/jquery.js" type="text/javascript"></script>

<script src="<?= base_url()?>/theme/datepicker.js" type="text/javascript"></script>

<script  type="text/javascript">

//==untuk validasi form
var FormControls = function () {
    //== Private functions
 
	var demo1 = function () {
        $( "#m_form_1" ).validate({
            // define validation rules
            rules: {
                nik: {
                    required: true,
                    digits: true 
                },
				alamat: {
                    required: true                     
                },
                nama: {
                    required: true 
                },
                jk: {
                    required: true
                },
                marital: {
                    required: true,
                },
                tanggal: {
                    required: true
                },
                provinsi: {
                    required: true
                },
                kabupaten: {
                    required: true
                },
                kecamatan: {
                    required: true
                },
				desa: {
                    required: true
                },
				pendidikan: {
                    required: true
                },
				pekerjaan: {
                    required: true
                },
            },
            
			//override massage
			messages: {
				nama: "Silahkan isi nama lengkap",
				nik: {
					required: "Silahkan isi nik",
					digits: "Silahkan isi dengan angka",
				},
				alamat: "Silahkan isi alamat lengkap",
				tanggal: "Silahkan isi tanggal lahir",
				provinsi: "Silahkan pilih provinsi",
				kabupaten: "Silahkan pilih kabupaten",
				pendidikan: "Silahkan pilih pendidikan",
				pekerjaan: "Silahkan isi password",
				desa: "Silahkan pilih desa",
				jk: "Silahkan pilih jenis kelamin",
				marital: "Silahkan pilih marital status ",
				kecamatan: "Silahkan pilih kecamatan",
			},
			
            //display error alert on form submit  
            invalidHandler: function(event, validator) {     
                var alert = $('#m_form_1_msg');
                alert.removeClass('m--hide').show();
                mUtil.scrollTop();
            },

            submitHandler: function (form) {
                form[0].submit(); // submit the form
				
            }
        });       
    }
 
		
	return {
        // public functions
        init: function() {
            demo1(); 
        }
    };
}();

//== untuk option with search
var Select2 = function() {
    //== Private functions
    var demos = function() {

        // basic
        $('#m_select2_1').select2({
            placeholder: "Pilih Provinsi",
            allowClear: true
        });

		// basic
        $('#m_select2_2').select2({
            placeholder: "Pilih Kabupaten",
            allowClear: true
        });
		
		// basic
        $('#m_select2_3',).select2({
            placeholder: "Pilih Privilege",
            allowClear: true
        });
		
		// basic
        $('#m_select2_4').select2({
            placeholder: "Pilih Status",
            allowClear: true
        });
		
		// basic
        $('#marital_status').select2({
            placeholder: "Pilih Marital Status",
            allowClear: true
        });
		// basic
        $('#jenis_kelamin').select2({
            placeholder: "Pilih Jenis Kelamin",
            allowClear: true
        });
		
		$('#m_pendidikan').select2({
            placeholder: "Pilih Pendidikan",
            allowClear: true
        });
		$('#m_desa').select2({
            placeholder: "Pilih desa",
            allowClear: true
        });
		$('#m_kecamatan').select2({
            placeholder: "Pilih kecamatan",
            allowClear: true
        });
    }


    //== Public functions
    return {
        init: function() {
            demos();
        }
    };
}();


	$(document).ready(function(){
		function get_kab($id,$param){
			$.ajax({
						type: "get",
						url: "<?= base_url() ?>user/get_data_by_id_json",
						data: "id=" + $id + "&param=" + $param,
						success: function (response) {
							//get_csrf();
							
							//console.log(response);
							var data= JSON.parse(response);
							
							if($param=='kabupaten'){
								var $el = $("#m_select2_2");
								$el.empty(); // remove old options
								$el.append('<option value="none">Pilih Kabupaten</option>');
								$.each(data,function(i,value){
									$el.append('<option value="'+value.kabid+'">'+value.name+'</option>');
								});
							}else if ($param=='kecamatan'){
								var $el = $("#m_kecamatan");
								$el.empty(); // remove old options
								$el.append('<option value="none">Pilih Kecamatan</option>');
								$.each(data,function(i,value){
									$el.append('<option value="'+value.kecid+'">'+value.name+'</option>');
								});
							}else if ($param=='desa'){
								var $el = $("#m_desa");
								$el.empty(); // remove old options
								$el.append('<option value="none">Pilih Desa</option>');
								$.each(data,function(i,value){
									$el.append('<option value="'+value.desaid+'">'+value.name+'</option>');
								});
							};
						}
					});
			
		}
		//panggil list kabupaten
		$('#m_select2_1').change(function(e){
			var thisId=$(this).val();
			//alert(thisId);
			
			get_kab(thisId,"kabupaten")
			
		});
		// panggil list kecamatan
		$('#m_select2_2').change(function(e){
			var thisId=$(this).val();
			//alert(thisId);
			
			get_kab(thisId,"kecamatan")
			
		});
		//panggil lis desa
		$('#m_kecamatan').change(function(e){
			var thisId=$(this).val();
			//alert(thisId);
			
			get_kab(thisId,"desa")
			
		});
		
		$('#batal').click(function(){
			window.location.href='<?= base_url() ?>user';
		});
		
		FormControls.init();
		Select2.init();
        
    });
</script>
