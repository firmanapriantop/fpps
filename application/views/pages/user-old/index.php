<div class="m-content">
	<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">
		<div class="m-alert__icon">
			<i class="flaticon-exclamation m--font-brand"></i>
		</div>
		<div class="m-alert__text">
			This example is almost identical to text based individual column example and provides the same functionality.
			With server-side processing enabled, all paging, searching, ordering actions that DataTables performs are handed off to a server where an SQL engine (or similar) can perform these actions on the large data set.
			See official documentation <a href="https://datatables.net/examples/api/multi_filter_select.html" target="_blank">here</a>.
		</div>
	</div>
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Daftar Pengguna
					</h3>
				</div>
			</div>
			<div class="m-portlet__head-tools">
				<button type="button" class="btn btn-primary" id="btn_add">Tambah Data</button>
			</div>
		</div>
		<div class="m-portlet__body" id="data">

			

		</div>
	</div>

	<!-- END EXAMPLE TABLE PORTLET-->
</div>

<script src="<?= base_url()?>/theme/vendors/jquery/dist/jquery.js" type="text/javascript"></script>
<script src="<?= base_url()?>/assets/muds.js" type="text/javascript"></script>

<script>
    $(document).ready(function(){
				LoadData("<?= base_url('user/load') ?>", "data");
    });
</script> 

