<!--begin::Modal-->
<div class="modal fade" id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="m-form m-form--fit" action="#" id="htmlForm" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="form-group">
                        <label class="form-control-label">Nama Lengkap:</label>
                        <input type="text" class="form-control" id="fullname" name="fullname" value="<?= $data->fullname ?>">
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">No Seluler:</label>
                        <input type="text" class="form-control" id="no_hp" name="no_hp" value="<?= $data->no_hp ?>">
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">Alamat Email:</label>
                        <input type="text" disabled="disabled" class="form-control" value="<?= $data->email ?>">
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">Password:</label>
                        <input type="password" class="form-control" id="password" name="password">
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">Password Lagi:</label>
                        <input type="password" class="form-control" id="rpassword" name="rpassword">
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">Role Access:</label>
                        <select class="form-control m-input" name="role_id" id="role_id">
                            <option value="<?= $data->role_id ?>"><?= $data->role ?></option>
                            <?php 
                                foreach($role->result() AS $row):
                                    echo '<option value="'.$row->roleid.'">'.$row->role.'</option>';
                                endforeach;
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">Status:</label>
                        <select class="form-control m-input" name="is_active" id="is_active">
                            <option value="<?= $data->is_active ?>"><?=  $data->nama_is_active ?></option>
                            <option value="1">Aktif</option>
                            <option value="0">Non Aktif</option>
                        </select>
                    </div>
                    <input type="hidden" class="form-control" id="userid" name="userid" value="<?= $data->userid ?>">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!--end::Modal-->

<script>
    $(document).ready(function(){
        
		$('#htmlForm').submit(function(e) {
			e.preventDefault();

			var me 			    = $(this);
            
            var userid      = $("#userid").val();
            var fullname    = $("#fullname").val();
            var no_hp       = $("#no_hp").val();
            var password    = $("#password").val();
            var rpassword   = $("#rpassword").val();
            var role_id     = $("#role_id").val();
            var is_active   = $("#is_active").val();
			
            var form_data 	= new FormData();
        
            form_data.append('userid', userid);
            form_data.append('fullname', fullname);
            form_data.append('no_hp', no_hp);
            form_data.append('password', password);
            form_data.append('rpassword', rpassword);
            form_data.append('role_id', role_id);
            form_data.append('is_active', is_active);

            $.ajax({
                url: '<?= base_url('user/save') ?>',
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: function(response){
                    if (response.success == true) {
						$('#modalAdd').modal('hide');
						LoadData("<?= base_url('user/load') ?>", "data");
						//notif("Informasi", "Data berhasil disimpan.");
					}
					else {
						$.each(response.messages, function(key, value) {
							var element = $('#' + key);
							
							element.closest('div.form-group')
							.removeClass('has-danger')
							.addClass(value.length > 0 ? 'has-danger' : 'has-success')
							.find('.text-danger')
							.remove();

							element.after(value);
						});
					}
                }
            });
        });
        
	});
</script>
