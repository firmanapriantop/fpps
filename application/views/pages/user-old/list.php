<!--begin: Datatable -->
<table class="table table-striped- table-bordered table-hover" id="tabel">
    <thead>
        <tr>
            <th></th>
            <th>Name</th>
            <th>Email</th>
            <th>Role</th>
            <th>Status</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php $no=0; foreach ($data->result() as $row): $no++; ?>
        <tr>
            <td><?= $no; ?></td>
            <td><?= $row->name; ?></td>
            <td><?= $row->email; ?></td>
            <td><?= $row->role; ?></td>
            <td>
                <?php 
                    if ($row->is_active == 'Aktif') {
                        echo '<span class="m-badge m-badge--success m-badge--wide">'.$row->is_active.'</span>';
                    } else {
                        echo '<span class="m-badge m-badge--danger m-badge--wide">'.$row->is_active.'</span>';
                    }
                ?>
            </td>
            <td>
                <span class="dropdown">
                    <a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="true"><i class="la la-ellipsis-h"></i></a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="#" onclick="CallModal('<?= base_url('user/edit/'.$row->userid) ?>', 'tmpModal', 'modalAdd')"><i class="la la-edit"></i> Edit</a>
                        <a class="dropdown-item text-danger" href="#"><i class="la la-print text-danger"></i> Hapus</a>
                    </div>
                </span>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<script>
	$(document).ready(function(){
		$('#tabel thead tr').clone(true).appendTo( '#tabel thead' );
        $('#tabel thead tr:eq(1) th').each( function (i) {
			var title = $(this).text();
			switch(title){
				case"Name":
				case"Email":
				case"Role":
				case "Status":
					$(this).html( '<input type="text" class="form-control form-control-sm form-filter m-input" placeholder="Search '+title+'" />' );
				break;
			};

			$( 'input', this ).on( 'keyup change', function () {
                if ( table.column(i).search() !== this.value ) {
                        table
                            .column(i)
                            .search( this.value )
                            .draw();
                }
			});
        });
 
        var table = $('#tabel').DataTable({
            orderCellsTop: true,
            fixedHeader: true,
            responsive:!0,
            "columnDefs": [{
                "targets"  : -1,
                "orderable": false
            }]
		});
		
		$('#btn_add').click(function(){
			CallModal("<?= base_url('user/add') ?>", "tmpModal", "modalAdd");
		});  
  });
</script>