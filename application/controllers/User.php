<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	var $tmp_path = 'templates/index';
    var $main_path = 'pages/user/';

    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('masuk') != TRUE){

            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">You need login first.</div>');

            redirect('auth/index');
        }

		$this->load->model(array('general_model' => 'gm','Sys_user_m'=>'user_m'));
		$this->load->library('form_validation');
    }

    public function index()
    {
        

        $data["isActive"]   = 'user';
		//$data['CSS_JS'] 	= asset_url('user');
       // $data["content"]    = 'user/index';
        
        
		
		$data['page'] = $this->main_path . 'index';
        
        $this->load->view($this->tmp_path, $data);

	}
	
	public function show_data()
	{
		$query = "
            select * from sys_user_vw
            ";

		$data['data']	= $this->gm->get_data($query);

		$this->load->view($this->main_path.'list', $data);
	}
	
	public function add_data()
    {

        $data["isActive"]   = 'user';
		//$data['CSS_JS'] 	= asset_url('dashboard');
		//$data["content"]    = 'user/add';
					
		$data['get_prov'] = $this->gm->get_all_prov();
		$data['get_role'] = $this->gm->get_all_role();
		$data['page'] = $this->main_path . 'add';
        
        $this->load->view($this->tmp_path, $data);
    }
	
	public function edit()
    {
		$id=$this->uri->segment(3);
        $data["isActive"]   = 'user';
		
		$data['data'] = $this->user_m->get_by_id($id);			
		$data['get_prov'] = $this->gm->get_all_prov();
		
		$provid=$data['data']->provid;
		
		$data['get_kab'] = $this->gm->get_all_kab_by_provId($provid);
		$data['get_role'] = $this->gm->get_all_role();
		$data['page'] = $this->main_path . 'edit';
        
        $this->load->view($this->tmp_path, $data);
    }
	
	public function save(){
		try{
			//$data["content"] = 'content/alokasi/index';
			$data['isActive'] = 'user';
			
			$id=$this->uri->segment(3);
			$image="default.png";
			
				if ($this->input->post('password')==""){
					$dataForm = array(
						'fullname' => $this->input->post('nama'),
						'no_hp' => $this->input->post('phone'),
						'kabid' => $this->input->post('kabupaten'),
						'email' => $this->input->post('email'),
						'image' => $image,
						'role_id' => $this->input->post('privilege'),
						'is_active' => $this->input->post('status'),
						'date_created' => date('Y-m-d H:m:s')
					);
				}else{
					$dataForm = array(
						'fullname' => $this->input->post('nama'),
						'no_hp' => $this->input->post('phone'),
						'kabid' => $this->input->post('kabupaten'),
						'email' => $this->input->post('email'),
						'image' => $image,
						'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
						'role_id' => $this->input->post('privilege'),
						'is_active' => $this->input->post('status'),
						'date_created' => date('Y-m-d H:m:s')
					);
				}				
			
			
			//print_r($dataForm);
			$this->user_m->set_data($id,$dataForm);
			redirect('user');
			
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
    }
	
	public function delete(){
		try{
			$id=$this->uri->segment(3);
			
			$this->user_m->delete_data($id);			
			
			redirect('user');
			
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
    }
	
	public function get_data_by_id_json() {
        try{
			$data="";
			$id = $this->input->get('id');
			//echo $this->input->get('param').'test';
		if ($this->input->get('param')=='kabupaten'){
			$data = $this->gm->get_all_kab_by_provId($id);
		}elseif($this->input->get('param')=='kecamatan'){
			$data = $this->gm->get_all_kec_by_kabId($id);
		}elseif($this->input->get('param')=='desa'){
			$data = $this->gm->get_all_desa_by_kecId($id);
		}
			
			echo json_encode($data);
			//redirect('alokasi');
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	public function profile()
    {
        $data['user'] = $this->db->get_where('sys_user', ['email'=> $this->session->userdata('email')])->row_array();

        $data["isActive"]   = 'dashboard';
		$data['CSS_JS'] 	= asset_url('dashboard');
		$data['page'] = $this->main_path . 'profile';

		$userid = $this->session->userdata('userid');

		$query=
			"
			select 
				u.userid, 
				u.fullname, 
				u.email,
				u.no_hp,
				u.image,
				u.date_created,
				u.kabid as kabid,
				r.role,
				k.name as kab,
				p.provid as provid,
				p.name as prov
			from 
				sys_user u
			left join sys_role r on r.roleid = u.role_id 
			left join ms_wil_kabupaten k on k.kabid = u.kabid 
			left join ms_wil_provinsi p on p.provid = k.provid
			where u.userid = $userid
			";

		$data['hasil'] = $this->gm->get_data($query)->row();
		$provid = $this->gm->get_data($query)->row('provid');

		$data['get_prov'] = $this->gm->get_data("select * from ms_wil_provinsi order by code");
		$data['get_kab'] = $this->gm->get_data("select * from ms_wil_kabupaten where provid = '$provid'");
		
		$this->load->view('templates/index', $data);
	}
	
	public function profile_save()
	{
		$this->session->unset_userdata('message');
		//$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">You have been logout.</div>');

		$status = array('success' => false, 'messages' => array());

		$this->form_validation->set_rules("fullname", "Full name", "trim|required");
		$this->form_validation->set_rules("email", "Email", "trim|required");
		$this->form_validation->set_rules("kabid", "Kabupaten", "trim|required");
		$this->form_validation->set_rules("no_hp", "Phone Number", "trim|required");
        
		$this->form_validation->set_message('required', '%s harus diisi');
		//$this->form_validation->set_message('required', ' ');
		$this->form_validation->set_message('is_natural_no_zero', '%s harus diisi dengan angka dan lebih dari 0');

		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');

		if ($this->form_validation->run() == FALSE) {
			foreach ($_POST as $key => $value) {
				$status['messages'][$key] = form_error($key);
			}
			//echo validation_errors();
		} else { //validasi benar semua

			//email checking
			$email = $this->session->userdata('email');
			$userid = $this->session->userdata('userid');
			
			if ($email != $this->input->post('email')) {
				$cek_email = $this->gm->get_data("select email from sys_user where email = '$email'")->num_rows();
				if ($cek_email > 0 ) {
					$status['messages']['email'] = '<p class="text-danger">Email sudah terdaftar</p>';
				}
			} else {
				
				$data 					= $_POST;
				$data['date_modified']	= date('Y-m-d H:i:s');

				$result				= $this->gm->save_data('sys_user', 'userid', $userid, $data);
				$status['success']  = true;

			}
			
		}
		
		echo json_encode($status);
	}

	public function profile_save_pass()
	{
		$status = array('success' => false, 'messages' => array());

		$this->form_validation->set_rules("old_pass", "Old Password", "trim|required");
		$this->form_validation->set_rules('new_pass', 'New Password', 'required|trim|min_length[3]|matches[confirmed_pass]', ['matches'=>'Password dont match', 'min_length'=>'Password too short']);
		$this->form_validation->set_rules('confirmed_pass', 'ConfirmePassword', 'required|trim|matches[new_pass]');

		$this->form_validation->set_message('required', '%s required');
		//$this->form_validation->set_message('required', ' ');
		$this->form_validation->set_message('is_natural_no_zero', '%s harus diisi dengan angka dan lebih dari 0');

		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');

		if ($this->form_validation->run() == FALSE) {
			foreach ($_POST as $key => $value) {
				$status['messages'][$key] = form_error($key);
			}
			//echo validation_errors();
		} else { //validasi benar semua
			$email = $this->session->userdata('email');
			$user = $this->db->get_where('sys_user', ['email' => $email])->row_array();
			$userid = $this->session->userdata('userid');
			//var_dump($user);
			$old_pass = $this->input->post('old_pass');

			if( password_verify($old_pass, $user['password']) ) {
				$data['password'] =  password_hash($this->input->post('new_pass'), PASSWORD_DEFAULT);
				$result				= $this->gm->save_data('sys_user', 'userid', $userid, $data);
				$status['success']  = true;

			} else {
				$status['messages']['old_pass'] = '<p class="text-danger">Password dont match.</p>';
			}
		}

		echo json_encode($status);
	}

	public function get_data_kab_by_provid()
	{
		$provid = $this->uri->segment(3);
		$data = $this->gm->get_all_kab_by_provId($provid);
		echo json_encode($data);
	}
	
}