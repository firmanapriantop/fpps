<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usulan extends CI_Controller {

	var $tmp_path = 'templates/index';
    var $main_path = 'pages/usulan/';

    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('masuk') != TRUE){

            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">You need login first.</div>');

            redirect('auth/index');
        }

        $this->load->model(array('general_model' => 'gm','usulan_m'=>'um'));
    }

    public function index()
    {    
     
		
		$data["isActive"]   = 'usulan';
		$data['page'] = $this->main_path . 'index';
        
		$this->load->view($this->tmp_path, $data);
		

    }
	
	function show_data()
	{
      
		$data["isActive"]   = 'usulan';
        
		$userid = $this->session->userdata('userid');
		
		if ($this->session->userdata('role_id')=='3'){
		
			$query = "
				select * from usulan_vw where createdby=$userid
				";
		}else {
			$query = "
            select * from usulan_vw
            ";			
		}

		$data['data']	= $this->gm->get_data($query);
		
		$this->load->view($this->main_path.'list', $data);
        
    }
	
	
	public function add()
    {

        $data["isActive"]   = 'usulan';
		$provid = $this->session->userdata('provid');
		$kabid =$this->session->userdata('kabid');
		
		if ($this->session->userdata('role_id')=='3'){
			$data['get_prov'] = $this->gm->get_all_prov($provid);
		}else{
			$data['get_prov'] = $this->gm->get_all_prov();
		}
		$data['get_komoditas'] = $this->um->get_all_komoditas();		
		$data['get_bank'] = $this->um->get_all_bank();
		$data['get_kat_pemanfaatan'] = $this->um->get_all_kat_pemanfaatan();
		$data['page'] = $this->main_path . 'add';
        echo $provid.$this->session->userdata('role_id');
        $this->load->view($this->tmp_path, $data);
    }
	
	public function edit()
    {
		$id=$this->uri->segment(3);
        $data["isActive"]   = 'usulan';
		
		$data['data'] = $this->um->get_by_id($id);			
		$data['get_komoditas'] = $this->um->get_all_komoditas();		
		
		$data['get_kat_pemanfaatan'] = $this->um->get_all_kat_pemanfaatan();
		if ($this->session->userdata('role_id')=='3'){
			
			$provid = $this->session->userdata('provid');
			$kabid =$this->session->userdata('kabid');
			
			$data['get_prov'] = $this->gm->get_all_prov($provid);
		}else{
			$data['get_prov'] = $this->gm->get_all_prov();
		}
		$data['get_bank'] = $this->um->get_all_bank();
		
		$provid=$data['data']->provid;
		
		
		
		$data['get_kab'] = $this->gm->get_all_kab_by_provId($provid);
		$kabid=$data['data']->kabid;
		
		$data['get_kec'] = $this->gm->get_all_kec_by_kabId($kabid);
		$kecid=$data['data']->kecid;

		$data['get_desa'] = $this->gm->get_all_desa_by_kecId($kecid);
		$desaid=$data['data']->desaid;
		
		$data['get_debitur'] = $this->gm->get_all_debitur_by_desaId($desaid);
		
		$data['page'] = $this->main_path . 'edit';
        
		//echo $provid.$kabid.$kecid;
        $this->load->view($this->tmp_path, $data);
    }
	
	public function save(){
		try{
			//$data["content"] = 'content/alokasi/index';
			$data['isActive'] = 'usulan';
			
			$id=$this->uri->segment(3);
			$xData="";
			
			if($this->input->post('status')==1){
				$realisasi = $this->input->post('realisasi');
				$no_spk= $this->input->post('noSPK');
			}else{
				$realisasi = "";
				$no_spk= "";
			}
			
			$tgl=tglSql($this->input->post('periode'));
			
			$dataForm = array(
						'debiturid' => $this->input->post('debitur'),
						'komoditasid' => $this->input->post('komoditas'),
						'agunan' => $this->input->post('agunan'),
						'ajuan_kredit' => $this->input->post('ajuan'),
						'periode' => $tgl,
						'pemanfaatanid' => $this->input->post('pemanfaatan'),
						'jangka_kredit' => $this->input->post('jangka'),
						'realisasi' => $realisasi,
						'no_spk' => $no_spk,
						'bankid' => $this->input->post('bank'),
						'status' => $this->input->post('status'),
						'keterangan' => $this->input->post('keterangan'),
						'tahun' => date("Y"),
					);
				if ($id==""){
					$dataForms = array(
						
						'createdby' => $this->session->userdata('userid'),
						'created_date' => date('Y-m-d H:m:s')
					);
				}else{
					$dataForms = array(
					
						'modifiedby' => $this->session->userdata('userid'),
						'modified_date' => date('Y-m-d H:m:s')
					);
				}				
			
			$xData=array_merge($dataForm,$dataForms);
			
			//print_r($xData);
			$this->um->set_data($id,$xData);
			redirect('usulan');
			
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
    }
	
	public function delete(){
		try{
			$id=$this->uri->segment(3);
			
			$this->um->delete_data($id);			
			
			redirect('usulan');
			
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
    }
	
	public function get_data_by_id_json() {
        try{
			$id = $this->input->get('provinsi_id');
			//echo $id.'test';
			
			$data = $this->gm->get_all_kab_by_provId($id);
			echo json_encode($data);
			//redirect('alokasi');
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
}