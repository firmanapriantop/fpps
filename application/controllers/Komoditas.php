<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Komoditas extends CI_Controller {

	var $tmp_path = 'templates/index';
    var $main_path = 'pages/komoditas/';

    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('masuk') != TRUE){

            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">You need login first.</div>');

            redirect('auth/index');
        }

        $this->load->model(array('general_model' => 'gm','Ms_komoditas_m'=>'komoditas_m'));
    }

	public function index()
    {    
     
		
		$data["isActive"]   = 'komoditas';
		$data['page'] = $this->main_path . 'index';
        
		$this->load->view($this->tmp_path, $data);
		

    }
	
	function show_data()
	{
      
		$data["isActive"]   = 'komoditas';
        
        $query = "
            select komoditasid,jenis,keterangan from ms_komoditas order by jenis
            ";

		$data['data']	= $this->gm->get_data($query);
		
		$this->load->view($this->main_path.'list', $data);
        
    }
	
	public function add()
    {

        $data["isActive"]   = 'komoditas';
		//$data['CSS_JS'] 	= asset_url('dashboard');
		//$data["content"]    = 'user/add';
					
		$data['get_prov'] = $this->komoditas_m->get_all_data();
		$data['page'] = $this->main_path . 'add';
			
        $this->load->view($this->tmp_path, $data);
    }
	
	public function edit()
    {
		$id=$this->uri->segment(3);
        $data["isActive"]   = 'komoditas';
		
		$data['data'] = $this->komoditas_m->get_by_id($id);	
		
		
		$data['page'] = $this->main_path . 'edit';
        
        $this->load->view($this->tmp_path, $data);
    }
	
	public function save(){
		try{
			//$data["content"] = 'content/alokasi/index';
			$data['isActive'] = 'user';
			
			$id=$this->uri->segment(3);
			
			
				if ($id==""){
					$dataForm = array(
						'Jenis' => $this->input->post('komoditas'),
						'Keterangan' => $this->input->post('keterangan'),
						'date_created' => date('Y-m-d H:m:s')
					);
				}else{
					$dataForm = array(
						'Jenis' => $this->input->post('komoditas'),
						'Keterangan' => $this->input->post('keterangan'),
						'date_modified' => date('Y-m-d H:m:s')
					);
				}				
			
			
			//print_r($dataForm);
			$this->komoditas_m->set_data($id,$dataForm);
			redirect('komoditas');
			
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
    }
	
	public function delete(){
		try{
			$id=$this->uri->segment(3);
			
			$this->komoditas_m->delete_data($id);			
			
			redirect('komoditas');
			
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
    }
	

}