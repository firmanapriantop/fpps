<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('general_model' => 'gm'));
		
		if($this->session->userdata('masuk') != TRUE){

            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">You need login first.</div>');

            redirect('auth/index');
        }
	}

	public function index()
	{
		
		$data["isActive"]   = 'dashboard';
		$data['CSS_JS'] 	= asset_url('dashboard');
		$data["page"]    	= 'dashboard/index';

		$query=
			"select distinct(tahun) from tx_usulan order by tahun";
		$data['tahun']=$this->gm->get_data($query);

		

		$this->load->view('templates/index', $data);
	}

	public function load_info_box()
	{
		$tahun = $this->uri->segment(3);

		//if admin
		$userid = $this->session->userdata('userid');
		$kabid = $this->session->userdata('kabid');

		if ($this->session->userdata('role_id') == '1' || $this->session->userdata('role_id') == '2') {
			$WHERE = 'where u2.role_id = 3';
			$WHERE2 = 'where year(created_date) ='.$tahun.'';
		} else {
			$query = 
			"select * from ms_wil_kabupaten where kabid = $kabid";
			$kab_code = $this->gm->get_data($query)->row('code');
			$prov_code = $this->gm->get_data($query)->row('provid');

			$data['jlh'] = substr($kab_code, 2, 2);
			if (substr($kab_code, 2, 2) == '00') {
				$WHERE = 'where u2.role_id = 3 and u2.provid = '.$prov_code.'';
			} else {
				$WHERE = 'where u2.role_id = 3 and u2.kabid = '.$kabid.'';
			}
			$WHERE2 = ' where createdby = '.$userid.' and year(created_date) = '.$tahun.'';
		}

		$query = 
			"select count(u2.userid) as jlh
			from (select 
						u1.userid,
						u1.fullname, 
						u1.email,
						u1.role_id as role_id,
						r1.role as role,
						kab.code as kab_code,
						kab.name as kab_nama,
						kab.provid as provid,
						kab.kabid as kabid
					from sys_user u1
					inner join sys_role r1 on r1.roleid = u1.role_id
					left join ms_wil_kabupaten kab on kab.kabid = u1.kabid 
					) u2
			$WHERE
			";

		//var_dump($query);

		$data['jlh_user'] = $this->gm->get_data($query)->row('jlh');

		$query = 
			"select 
				year(created_date) as tahun_date, count(usulanid) as jlh
			from 
				tx_usulan 
			$WHERE2
			group by tahun_date
			";
			
		$data['jlh_usulan'] = $this->gm->get_data($query)->row('jlh');

		$query = 
			"select 
				year(created_date) as tahun_date, sum(ajuan_kredit) as jlh
			from tx_usulan 
			$WHERE2
			group by tahun_date
			";
			
		$data['jlh_ajuan_kredit'] = $this->gm->get_data($query)->row('jlh');

		$query = 
			"select 
				year(created_date) as tahun_date, sum(realisasi) as jlh
			from tx_usulan 
			$WHERE2
			group by tahun_date
			";
			
		$data['jlh_realisasi'] = $this->gm->get_data($query)->row('jlh');

		$this->load->view('dashboard/info_box', $data);
	}

	public function load_bar_chart()
	{
		$tahun = $this->uri->segment(3);

		//if admin
		$userid = $this->session->userdata('userid');
		$kabid = $this->session->userdata('kabid');

		if ($this->session->userdata('role_id') == '1' || $this->session->userdata('role_id') == '2') {
			
			$WHERE = 'where tahun_date = '.$tahun.'';


		} else {

			$WHERE = 'where createdby = '.$userid.' and tahun_date = '.$tahun.'';
		}

		$query = 
			"select
				bulan,
				case
					when bulan = 1 then 'Januari' 
					when bulan = 2 then 'Februari' 
					when bulan = 3 then 'Maret' 
					when bulan = 4 then 'April' 
					when bulan = 5 then 'Mei'
					when bulan = 6 then 'Juni' 
					when bulan = 7 then 'Juli' 
					when bulan = 8 then 'Agustus' 
					when bulan = 9 then 'September' 
					when bulan = 10 then 'Oktober' 
					when bulan = 11 then 'November' 
					when bulan = 12 then 'Desember' 
				end as bulan2,
				tahun_date,
				createdby, 
				sum(ajuan_kredit) as ajuan_kredit, 
				sum(realisasi) as realisasi
			from
				(select 
					month(created_date) as bulan, 
					year(created_date) as tahun_date, 
					createdby, 
					ajuan_kredit,
					realisasi
				from tx_usulan) u
			$WHERE
			group by bulan, tahun_date, createdby
			";

		$data['hasil'] = $this->gm->get_data($query);

		$this->load->view('dashboard/bar_chart', $data);
	}

	public function load_line_chart()
	{
		$tahun = $this->uri->segment(3);

		//if admin
		$userid = $this->session->userdata('userid');
		$kabid = $this->session->userdata('kabid');

		if ($this->session->userdata('role_id') == '1' || $this->session->userdata('role_id') == '2') {
			
			$WHERE = 'where tahun_date = '.$tahun.'';


		} else {

			$WHERE = 'where createdby = '.$userid.' and tahun_date = '.$tahun.'';
		}

		$query = 
			"select 
				bulan, 
				case
					when bulan = 1 then 'Januari' 
					when bulan = 2 then 'Februari' 
					when bulan = 3 then 'Maret' 
					when bulan = 4 then 'April' 
					when bulan = 5 then 'Mei'
					when bulan = 6 then 'Juni' 
					when bulan = 7 then 'Juli' 
					when bulan = 8 then 'Agustus' 
					when bulan = 9 then 'September' 
					when bulan = 10 then 'Oktober' 
					when bulan = 11 then 'November' 
					when bulan = 12 then 'Desember' 
				end as bulan2,
				tahun_date, 
				createdby, 
				count(usulanid) as jlh 
			from 
				(select 
				month(created_date) as bulan, 
				year(created_date) as tahun_date, 
				createdby, 
				usulanid
			from tx_usulan) as u
			$WHERE
			group by bulan, tahun_date, createdby
			";

		$data['hasil_line'] = $this->gm->get_data($query);
			
		$this->load->view('dashboard/line_chart', $data);
	}

	function is_pusat()
	{

	}
	
}
