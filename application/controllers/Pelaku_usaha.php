<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pelaku_usaha extends CI_Controller {

	var $tmp_path = 'templates/index';
    var $main_path = 'pages/pelaku_usaha/';

    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('masuk') != TRUE){

            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">You need login first.</div>');

            redirect('auth/index');
        }

        $this->load->model(array('general_model' => 'gm','Pelaku_usaha_m'=>'pu_m'));
    }

    public function index()
    {    
     
		
		$data["isActive"]   = 'pelaku_usaha';
		$data['page'] = $this->main_path . 'index';
        
		$this->load->view($this->tmp_path, $data);
		

    }
	
	function show_data()
	{
      
		$data["isActive"]   = 'pelaku_usaha';
        
		$kabid = $this->session->userdata('kabid');
		
		if ($this->session->userdata('role_id')=='3'){	
			$query = "
				select * from ms_debitur_vw where kabid= $kabid order by nama
				";
		}else{
			$query = "
				select * from ms_debitur_vw order by nama
				";
		}
		//echo $query;
		$data['data']	= $this->gm->get_data($query);
		
		$this->load->view($this->main_path.'list', $data);
        
    }
	
	
  	
	public function add()
    {

        $data["isActive"]   = 'pelaku_usaha';
		//$data['CSS_JS'] 	= asset_url('dashboard');
		//$data["content"]    = 'user/add';
		
		$provid = $this->session->userdata('provid');
		$kabid =$this->session->userdata('kabid');
		
		if ($this->session->userdata('role_id')=='3'){
			$data['get_prov'] = $this->gm->get_all_prov($provid);
		}else{
			$data['get_prov'] = $this->gm->get_all_prov();
		}	
		$data['get_pendidikan'] = $this->pu_m->get_pendidikan();
		$data['get_marital'] = $this->pu_m->get_marital();
		$data['page'] = $this->main_path . 'add';
        
        $this->load->view($this->tmp_path, $data);
    }
	
	public function edit()
    {
		$id=$this->uri->segment(3);
        $data["isActive"]   = 'pelaku_usaha';
		
		
		
		$data['data'] = $this->pu_m->get_by_id($id);	
		$data['get_pendidikan'] = $this->pu_m->get_pendidikan();
		$data['get_marital'] = $this->pu_m->get_marital();		
		if ($this->session->userdata('role_id')=='3'){
			$provid = $this->session->userdata('provid');
			$kabid =$this->session->userdata('kabid');
			$data['get_prov'] = $this->gm->get_all_prov($provid);
		}else{

			$provid=$data['data']->provid;
			$data['get_prov'] = $this->gm->get_all_prov();
		}
		
		//$provid=$data['data']->provid;
		
		$data['get_kab'] = $this->gm->get_all_kab_by_provId($provid);
		$kabid=$data['data']->kabid;
		
		$data['get_kec'] = $this->gm->get_all_kec_by_kabId($kabid);
		$kecid=$data['data']->kecid;

		$data['get_desa'] = $this->gm->get_all_desa_by_kecId($kecid);
		
		
		$data['page'] = $this->main_path . 'edit';
        
        $this->load->view($this->tmp_path, $data);
    }
	
	public function save(){
		try{
			//$data["content"] = 'content/alokasi/index';
			$data['isActive'] = 'pelaku_usaha';
			
			$id=$this->uri->segment(3);
			$tgl=tglSql($this->input->post('tanggal'));
			
				if ($id==""){
					$dataForm = array(
						'nama' => $this->input->post('nama'),
						'nik' => $this->input->post('nik'),
						'tgl_lahir' => $tgl,
						'npwp' => $this->input->post('npwp'),
						'jenis_kelamin' => $this->input->post('jk'),
						'maritalid' => $this->input->post('marital'),
						'pendidikanid' => $this->input->post('pendidikan'),
						'pekerjaan' => $this->input->post('pekerjaan'),
						'desaid' => $this->input->post('desa'),
						'alamat_lengkap' => $this->input->post('alamat'),
						'date_created' => date('Y-m-d H:m:s')
					);
				}else{
					$dataForm = array(
						'nama' => $this->input->post('nama'),
						'nik' => $this->input->post('nik'),
						'tgl_lahir' => $tgl,
						'npwp' => $this->input->post('npwp'),
						'jenis_kelamin' => $this->input->post('jk'),
						'maritalid' => $this->input->post('marital'),
						'pendidikanid' => $this->input->post('pendidikan'),
						'pekerjaan' => $this->input->post('pekerjaan'),
						'desaid' => $this->input->post('desa'),
						'alamat_lengkap' => $this->input->post('alamat'),
						'date_modified' => date('Y-m-d H:m:s')
					);
				}				
			
			
			//print_r($dataForm);
			$this->pu_m->set_data($id,$dataForm);
			redirect('pelaku_usaha');
			
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
    }
	
	public function delete(){
		try{
			$id=$this->uri->segment(3);
			
			$this->pu_m->delete_data($id);			
			
			redirect('pelaku_usaha');
			
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
    }
	
	public function get_data_by_id_json() {
        try{
			$id = $this->input->get('desaid');
			//echo $id.'test';
			
			$data = $this->gm->get_all_debitur_by_desaId($id);
			echo json_encode($data);
			//redirect('alokasi');
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
}